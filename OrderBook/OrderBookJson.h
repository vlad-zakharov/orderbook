#pragma once
#include <nlohmann/json.hpp>
#include "OrderBookTypes.h"

/**
 * @brief JSON Serialization functions for Order Book types
 *
 */

using json = nlohmann::json;

namespace OB {

void to_json(json& j, const Level& p);
void from_json(const json& j, Level& p);

void to_json(json& j, const Order& p);
void from_json(const json& j, Order& p);

NLOHMANN_JSON_SERIALIZE_ENUM( OrderType, {
    {OrderType::invalid, nullptr},
    {OrderType::ask, "ask"},
    {OrderType::bid, "bid"}
})
}