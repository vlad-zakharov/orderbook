#pragma once
namespace OB {

using price_t = unsigned long long;
using quantity_t = unsigned long long;
using orderid_t = unsigned long long;

/**
 * @brief Represents an order type. Invalid is used for JSON serialization
 *
 */
enum class OrderType {
    invalid,
    ask,
    bid
};

/**
 * @brief An order with full information
 *
 */
struct Order {
    OrderType orderType;
    orderid_t orderId;
    price_t price;
    quantity_t quantity;
};
bool operator==(const Order &lhs, const Order &rhs);

/**
 * @brief A price level in the order book. Anonymized accumulated info without any track to orders
 *
 */
struct Level {
    price_t price;
    quantity_t quantity;
};
bool operator==(const Level &lhs, const Level &rhs);

/**
 * @brief Accumulated order book
 *
 * The first vector stores asks, the second vector stores bids
 *
 */
using MarketData = std::pair<std::vector<Level>, std::vector<Level>>;
}