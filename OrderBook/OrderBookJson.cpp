#include "OrderBookJson.h"

namespace OB {

void to_json(json& j, const Order& p) {
    j = json{ { "order", {
        {"id", p.orderId},
        {"type", p.orderType},
        {"price", p.price},
        {"quantity", p.quantity}
    } } };
}

void from_json(const json& j, Order& p) {
    j.at("id").get_to(p.orderId);
    j.at("type").get_to(p.orderType);
    j.at("price").get_to(p.price);
    j.at("quantity").get_to(p.quantity);
}

void to_json(json& j, const Level& p) {
    j = json{
        {"price", p.price},
        {"quantity", p.quantity}
    };
}

void from_json(const json& j, Level& p) {
    j.at("price").get_to(p.price);
    j.at("quantity").get_to(p.quantity);
}

}