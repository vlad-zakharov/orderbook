#include <algorithm>

#include "OrderBook.h"
#include "OrderBookTypes.h"

namespace OB {

bool OrderBook::postOrder(Order order) {
    // try to emplace an order and return false if emplace failed
    const auto emplaceResult = m_orders.emplace(order.orderId, order);
    if (!emplaceResult.second) {
        return false;
    }

    try {
        if (order.orderType == OrderType::ask) {
            emplaceOrAdd(m_asksMap, order);
        } else {
            emplaceOrAdd(m_bidsMap, order);
        }
    } catch (...) {
        // cancel the insertion to the m_orders map and propogate exception is smth failed
        m_orders.erase(emplaceResult.first);
        throw;
    }

    return true;
}

bool OrderBook::cancelOrder(orderid_t orderID) {
    const auto orderIt = m_orders.find(orderID);
    if (orderIt == m_orders.end()) {
        return false;
    }

    // decrease the quantity for this amount and delete the level if it's empty
    if (orderIt->second.orderType == OrderType::ask) {
        eraseOrSubstitute(m_asksMap, orderIt->second);
    } else {
        eraseOrSubstitute(m_bidsMap, orderIt->second);
    }

    m_orders.erase(orderIt);

    return true;
}

MarketData OrderBook::getMarketData() const {
    std::vector<Level> asks;
    std::vector<Level> bids;

    std::transform(m_asksMap.cbegin(), m_asksMap.cend(), std::back_inserter(asks),
        [](const std::pair<price_t, quantity_t> &el) {
            return Level{el.first, el.second};
        }
    );
    std::transform(m_bidsMap.crbegin(), m_bidsMap.crend(), std::back_inserter(bids),
        [](const std::pair<price_t, quantity_t> &el) {
            return Level{el.first, el.second};
        }
    );

    return std::make_pair(asks, bids);
}

std::optional<Order> OrderBook::getOrder(orderid_t orderID) const {
    auto order = m_orders.find(orderID);
    if (order != m_orders.end()) {
        return order->second;
    } else {
        return std::nullopt;
    }
}

void OrderBook::emplaceOrAdd(LevelMap &map, const Order order) {
    auto searchIt = map.find(order.price);
    if (searchIt != map.end()) {
        searchIt->second += order.quantity;
    } else {
        map.emplace(order.price, order.quantity);
    }
}

void OrderBook::eraseOrSubstitute(LevelMap &map, const Order order) {
    auto searchIt = map.find(order.price);
    if (searchIt != map.end()) {
        searchIt->second -= order.quantity;
    } else {
        throw std::logic_error("Order found but no such level price!");
    }

    if (searchIt->second == 0) {
        map.erase(searchIt);
    }
}

bool operator==(const Order &lhs, const Order &rhs) {
    return
        lhs.orderType == rhs.orderType &&
        lhs.orderId == rhs.orderId &&
        lhs.price == rhs.price &&
        lhs.quantity == rhs.quantity;
}

bool operator==(const Level &lhs, const Level &rhs) {
    return
        lhs.price == rhs.price &&
        lhs.quantity == rhs.quantity;
}

}