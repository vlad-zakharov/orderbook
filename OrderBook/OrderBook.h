#pragma once
#include <map>
#include <nlohmann/json.hpp>
#include <optional>
#include <vector>
#include <unordered_map>
#include <utility>

#include "OrderBookTypes.h"

namespace OB {

/**
 * @brief A class to handle orders and to accumulate them to form Marked Data
 *
 */
class OrderBook {
public:
    /**
     * @brief Post an order request
     *
     * @param order
     * @return true if the post succeded
     * @return false if the order was already in the book
     */
    bool postOrder(Order order);

    /**
     * @brief
     *
     * @param orderID of an order to cance
     * @return true if the cancel succeeded
     * @return false if there was no such order in the book
     */
    bool cancelOrder(orderid_t orderID);

    /**
     * @brief Get the Order object
     *
     * @param orderId
     * @return std::optional<Order>
     */
    std::optional<Order> getOrder(orderid_t orderId) const;

    /**
     * @brief Get the accumulated sorted Market Data as a pair of vectors
     *
     * The first vector contains asks, the second vector contains bid
     *
     * @return MarketDat
     */
    MarketData getMarketData() const;
private:
    using LevelMap = std::map<price_t, quantity_t>;
    using OrderRegistry = std::unordered_map<orderid_t, Order>;
    LevelMap m_asksMap;
    LevelMap m_bidsMap;
    OrderRegistry m_orders;

    static inline void emplaceOrAdd(LevelMap &map, const Order order);
    static inline void eraseOrSubstitute(LevelMap &map, const Order order);
};

}