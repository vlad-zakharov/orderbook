#pragma once
#include <memory>
#include <optional>
#include <restinio/all.hpp>
#include <string>
#include "OrderBook/OrderBook.h"

/**
 * @brief Router type alias
 *
 */
using router_t = restinio::router::express_router_t<>;
/**
 * @brief Represents a pointer to a router
 *
 */
using RouterPtr = std::unique_ptr<router_t>;

/**
 * @brief Server class represents a request handling instance
 *
 */
class Server {
public:

    /**
     * @brief Construct a new Server object
     *
     */
    Server() = default;

    /**
     * @brief The server is a non-copyable instance
     *
     */
    Server(const Server&) = delete;

    /**
     * @brief Initializes handlers and start request handling
     *
     */
    void run();
private:
    OB::OrderBook m_orderBook;

    RouterPtr initRequestHandler();
    restinio::request_handling_status_t
        handleOrderPost(restinio::request_handle_t req);
    restinio::request_handling_status_t
        handleOrderGet(restinio::request_handle_t req,
                       restinio::router::route_params_t params);
    restinio::request_handling_status_t
        handleOrderDelete(restinio::request_handle_t req,
                       restinio::router::route_params_t params);
    restinio::request_handling_status_t
        handleMarketDataGet(restinio::request_handle_t req);
};