#include <nlohmann/json.hpp>
#include <string>

#include "Server.h"
#include "OrderBook/OrderBook.h"
#include "OrderBook/OrderBookJson.h"

using json = nlohmann::json;
using namespace restinio;

namespace {
    const char *E_DUP_ORDER_ID =  R"-({"error" : "Duplicate order id"})-";
    const char *E_BAD_REQUEST =  R"-({"error" : "Bad request"})-";
    const char *E_ORDER_NOT_FOUND =  R"-({"error" : "Order not found"})-";
};

RouterPtr Server::initRequestHandler() {
    RouterPtr router = std::make_unique<router_t>();
    using namespace std::placeholders;

    router->http_post(
        "/book/orders",
        std::bind(&Server::handleOrderPost, this, _1)
    );

    router->http_get(
        "/book/orders/:id",
        std::bind(&Server::handleOrderGet, this, _1, _2)
    );

    router->http_delete(
        "/book/orders/:id",
        std::bind(&Server::handleOrderDelete, this, _1, _2)
    );

    router->http_get(
        "/book/market_data",
        std::bind(&Server::handleMarketDataGet, this, _1)
    );

   router->non_matched_request_handler(
      []( auto req ){
        return
          req->create_response(http_status_line_t{ http_status_code_t(404), "Not Found" })
            .connection_close()
            .done();
      } );

    return router;
}

void Server::run() {
    using traits_t =
        traits_t<
            asio_timer_manager_t,
            single_threaded_ostream_logger_t,
            router_t
        >;

    restinio::run(
        on_this_thread<traits_t>()
            .port(8080)
            .address("localhost")
            .request_handler(initRequestHandler())
    );
}

request_handling_status_t Server::handleOrderPost(request_handle_t req) {
    http_status_line_t statusLine;
    std::string responseBody;

    try {
        OB::Order order = json::parse(req->body()).get<OB::Order>();

        if (m_orderBook.postOrder(order)) {
            statusLine = status_ok();
            responseBody = json(order).dump();
        } else {
            statusLine = status_bad_request();
            responseBody = E_DUP_ORDER_ID;
        }
    } catch (json::out_of_range &e) {
        statusLine = status_bad_request();
        responseBody = E_BAD_REQUEST;
    } catch (json::type_error &e) {
        statusLine = status_bad_request();
        responseBody = E_BAD_REQUEST;
    } catch (...) {
        throw;
    }

    req->create_response(statusLine)
        .append_header( restinio::http_field::content_type, "text/html; charset=utf-8" )
        .set_body(responseBody)
        .done();

    return restinio::request_accepted();
}

request_handling_status_t Server::handleOrderGet(request_handle_t req, router::route_params_t params) {
    OB::orderid_t orderId = cast_to<OB::orderid_t>(params["id"]);

    http_status_line_t statusLine;
    std::string responseBody;

    const auto order = m_orderBook.getOrder(orderId);
    if (order.has_value()) {
        statusLine = status_ok();
        responseBody = json(order.value()).dump();
    } else {
        statusLine = status_bad_request();
        responseBody = E_ORDER_NOT_FOUND;
    }

    req->create_response(statusLine)
        .append_header( restinio::http_field::content_type, "text/html; charset=utf-8" )
        .set_body(responseBody)
        .done();

    return restinio::request_accepted();
}

request_handling_status_t Server::handleOrderDelete(request_handle_t req, router::route_params_t params) {
    OB::orderid_t orderId = cast_to<OB::orderid_t>(params["id"]);

    http_status_line_t statusLine;
    std::string responseBody;

    if (m_orderBook.cancelOrder(orderId)) {
        statusLine = status_ok();
    } else {
        statusLine = status_bad_request();
        responseBody = E_ORDER_NOT_FOUND;
    }

    req->create_response(statusLine)
        .append_header( restinio::http_field::content_type, "text/html; charset=utf-8" )
        .set_body(responseBody)
        .done();

    return restinio::request_accepted();
}


request_handling_status_t Server::handleMarketDataGet(request_handle_t req) {
    const OB::MarketData marketData = m_orderBook.getMarketData();

    json j{
        {"asks", marketData.first},
        {"bids", marketData.second}
    };

    req->create_response()
        .append_header( restinio::http_field::content_type, "text/html; charset=utf-8" )
        .set_body(j.dump())
        .done();

    return restinio::request_accepted();
}
