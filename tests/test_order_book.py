#!/usr/local/bin/python3
import requests
import unittest

REST_LINK='http://localhost:8080/book/'

def build_request(oType, id, price, quantity):
    return {
        'id': id,
        'type': oType,
        'price': price,
        'quantity': quantity
    }

class TestRestOrderBook(unittest.TestCase):
    def test_1_create(self):
        # Test post good request
        response = requests.post(REST_LINK + 'orders',
            json = build_request('ask', 1, 100, 1000))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json()['order']['id'], 1)

        # Test post duplicate order id
        response = requests.post(REST_LINK + 'orders',
            json = build_request('ask', 1, 100, 1000))
        self.assertEqual(response.status_code, 400)

        # Test post bad request
        response = requests.post(REST_LINK + 'orders',
            json = build_request('bid', 2, 'blabla', 100))
        self.assertEqual(response.status_code, 400)

    def test_2_cancel(self):
        # Add some order
        response = requests.post(REST_LINK + 'orders',
            json = build_request('bid', 4, 100, 100))
        self.assertEqual(response.status_code, 200)

        # Test delete bad request
        response = requests.delete(REST_LINK + 'orders/1234')
        self.assertEqual(response.status_code, 400)

         # Test delete good request
        response = requests.delete(REST_LINK + 'orders/1')
        self.assertEqual(response.status_code, 200)

    def test_3_market_data(self):
        # Add some orders
        response = requests.post(REST_LINK + 'orders',
            json = build_request('bid', 10, 500, 100))
        self.assertEqual(response.status_code, 200)

        response = requests.post(REST_LINK + 'orders',
            json = build_request('bid', 11, 500, 50))
        self.assertEqual(response.status_code, 200)

        response = requests.post(REST_LINK + 'orders',
            json = build_request('bid', 12, 500, 25))
        self.assertEqual(response.status_code, 200)

        # Test quantity is accumulated
        response = requests.get(REST_LINK + 'market_data')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json()['bids'][0]['price'], 500)
        self.assertEqual(response.json()['bids'][0]['quantity'], 175)

        # Test quantity decreases after delete
        response = requests.delete(REST_LINK + 'orders/11')
        self.assertEqual(response.status_code, 200)

        response = requests.get(REST_LINK + 'market_data')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json()['bids'][0]['price'], 500)
        self.assertEqual(response.json()['bids'][0]['quantity'], 125)

   
if __name__ == '__main__':
    unittest.main()
