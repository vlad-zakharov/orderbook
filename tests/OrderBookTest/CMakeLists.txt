add_executable(OrderBookTest
  ${PROJECT_SOURCE_DIR}/OrderBook/OrderBook.h
  ${PROJECT_SOURCE_DIR}/OrderBook/OrderBook.cpp
  OrderBookTestMain.cpp
  OrderBookTest.cpp
)

TARGET_LINK_LIBRARIES(OrderBookTest
    libgtest
)

add_test(NAME OrderBookTest
         COMMAND OrderBookTest)                                