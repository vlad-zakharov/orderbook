#include <cstdlib>
#include <ctime>

#include "OrderBook/OrderBook.h"
#include "OrderBookTest.h"

OrderBookTest::OrderBookTest() {}

OrderBookTest::~OrderBookTest() {}

TEST_F(OrderBookTest, PostOrderAddsOrder) {
    constexpr Order testBid{OrderType::bid, 1, 1000, 110};
    constexpr Order testAsk{OrderType::ask, 2, 1000, 90};

    orderBook.postOrder(testBid);
    orderBook.postOrder(testAsk);

    auto marketData = orderBook.getMarketData();

    const MarketData expectedData = {
        { { 1000, 90 } },
        { { 1000, 110 } }
    };

    EXPECT_EQ(expectedData, marketData);
}


TEST_F(OrderBookTest, PostOrderReturnsTrueIfNewOrderIsAdded) {
    constexpr Order testOrder1{OrderType::bid, 1, 1000, 100};

    EXPECT_TRUE(orderBook.postOrder(testOrder1));
}

TEST_F(OrderBookTest, PostOrderFailsIfOrderWithSuchIDIsPresent) {
    constexpr Order testOrder1{OrderType::bid, 1, 1000, 100};
    constexpr Order testOrder2{OrderType::ask, 1, 1000, 100};
    constexpr Order testOrder3{OrderType::ask, 1, 500, 100};

    EXPECT_TRUE(orderBook.postOrder(testOrder1));
    EXPECT_FALSE(orderBook.postOrder(testOrder2));
    EXPECT_FALSE(orderBook.postOrder(testOrder3));
}

TEST_F(OrderBookTest, GetOrderReturnsNothingIfNoSuchOrderId) {
    auto order = orderBook.getOrder(1);
    EXPECT_FALSE(order.has_value());

    constexpr Order testOrder{OrderType::ask, 1, 1000, 100};
    orderBook.postOrder(testOrder);

    order = orderBook.getOrder(2);
    EXPECT_FALSE(order.has_value());
}

TEST_F(OrderBookTest, GetOrderReturnsOrderWithCorrespondingOrderId) {
    constexpr Order testOrder{OrderType::ask, 1, 1000, 100};
    orderBook.postOrder(testOrder);

    auto order = orderBook.getOrder(1);
    EXPECT_TRUE(order.has_value());
    EXPECT_EQ(testOrder, order.value());
}

TEST_F(OrderBookTest, PostOderIncreasesQuantityForTheSamePrice) {
    constexpr Order testOrder1{OrderType::ask, 1, 1000, 100};
    constexpr Order testOrder2{OrderType::ask, 2, 1000, 50};

    orderBook.postOrder(testOrder1);
    orderBook.postOrder(testOrder2);

    auto marketData = orderBook.getMarketData();
    const MarketData expectedData = {
        { { 1000, 150 } },
        { }
    };

    EXPECT_EQ(expectedData, marketData);
}

TEST_F(OrderBookTest, MarketDataIsSorted) {
    std::srand(std::time(0));
    for (std::size_t i = 1; i <= 100; ++i) {
        orderBook.postOrder({OrderType::ask, i, 1000u + (std::rand() % 1000u), 100});
        orderBook.postOrder({OrderType::bid, 200 + i, 1000u - (std::rand() % 1000u), 100});
    }
    auto markedData = orderBook.getMarketData();

    auto previous = markedData.first.at(0);
    for (std::size_t i = 1; i < markedData.first.size(); ++i) {
        EXPECT_LE(previous.price, markedData.first[i].price);
        previous = markedData.first[i];
    }

    previous = markedData.second.at(0);
    for (std::size_t i = 1; i < markedData.second.size(); ++i) {
        EXPECT_GE(previous.price, markedData.second[i].price);
        previous = markedData.second[i];
    }
}
