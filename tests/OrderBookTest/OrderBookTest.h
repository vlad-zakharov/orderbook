#pragma once
#include <gtest/gtest.h>
#include "OrderBook/OrderBook.h"

using namespace testing;
using namespace OB;

class OrderBookTest: public Test {
protected:
   OrderBookTest();
   virtual ~OrderBookTest();

   OrderBook orderBook;
};
